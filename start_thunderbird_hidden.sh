#!/bin/bash

thunderbird &
while [[ $(wmctrl -l | grep Thunderbird) == "" ]]; do sleep 0.1; done
wmctrl -r Thunderbird -b add,hidden
wmctrl -k on
